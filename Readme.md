## Survey Builder Service

## Requerimients
* Node v7.6.0 or higher.

### Environment variables
Configure the environment variables in bash using `export {ENV_VAR_NAME}={VALUE}`

* `SURVEY_DB_HOST`: Database host (default `127.0.0.1`)
* `SURVEY_DB_USER`: Database user.
* `SURVEY_DB_PASS`: Database password.
* `SURVEY_DB_NAME`: Database name.
* `SURVEY_DB_PORT`: Database port.
  
## Installation
On project directory excecute
```
$ npm install
```

## Running server
```
$ npm run watch 
```
or
``` 
$ npm run dev
