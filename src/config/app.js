export default {
  DEFAULT_PORT: 3000,

  APP_NAME: 'Survey-Builder Service',

  APP_DOMAIN: process.env.AUTH_APP_DOMAIN || 'http://localhost',
};
