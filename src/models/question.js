const Knex = require('knex');
const { Model } = require('objection');
const knexfile = require('../config/knexfile');

const connection = Knex(knexfile);

Model.knex(connection);

class Question extends Model {
  static get tableName() {
    return 'questions';
  }

  static get relationMappings() {
    const Group = require('./group').default;
    const Poll = require('./poll').default;
    const Choice = require('./choice').default;

    return {
      group: {
        relation: Model.BelongsToOneRelation,
        modelClass: Group,
        join: {
          from: 'questions.group_id',
          to: 'groups.id',
        },
      },
      poll: {
        relation: Model.HasOneThroughRelation,
        modelClass: Poll,
        join: {
          from: 'questions.group_id',
          through: {
            from: 'groups.id',
            to: 'groups.poll_id',
          },
          to: 'polls.id',
        },
      },
      choices: {
        relation: Model.HasManyRelation,
        modelClass: Choice,
        join: {
          from: 'questions.id',
          to: 'choices.question_id',
        },
      },
    };
  }
}

export default Question;
