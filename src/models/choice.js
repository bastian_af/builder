const Knex = require('knex');
const { Model } = require('objection');
const knexfile = require('../config/knexfile');

const connection = Knex(knexfile);

Model.knex(connection);

class Choice extends Model {
  static get tableName() {
    return 'choices';
  }

  static get relationMappings() {
    const Question = require('./question').default;

    return {
      question: {
        relation: Model.BelongsToOneRelation,
        modelClass: Question,
        join: {
          from: 'choices.question_id',
          to: 'questions.id',
        },
      },
    };
  }
}

export default Choice;
