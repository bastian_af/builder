const Knex = require('knex');
const { Model } = require('objection');
const knexfile = require('../config/knexfile');

const connection = Knex(knexfile);

Model.knex(connection);

class Group extends Model {
  static get tableName() {
    return 'groups';
  }

  static get relationMappings() {
    const Poll = require('./poll').default;
    const Question = require('./question').default;

    return {
      poll: {
        relation: Model.BelongsToOneRelation,
        modelClass: Poll,
        join: {
          from: 'groups.poll_id',
          to: 'polls.id',
        },
      },
      questions: {
        relation: Model.HasManyRelation,
        modelClass: Question,
        join: {
          from: 'groups.id',
          to: 'questions.group_id',
        },
      },
    };
  }
}

export default Group;
