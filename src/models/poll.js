const Knex = require('knex');
const { Model } = require('objection');
const knexfile = require('../config/knexfile');

const connection = Knex(knexfile);

Model.knex(connection);

class Poll extends Model {
  static get tableName() {
    return 'polls';
  }

  static get relationMappings() {
    const Group = require('./group').default;

    return {
      groups: {
        relation: Model.HasManyRelation,
        modelClass: Group,
        join: {
          from: 'polls.id',
          to: 'groups.poll_id',
        },
      },
    };
  }
}

export default Poll;
