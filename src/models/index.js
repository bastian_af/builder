import Poll from './poll';
import Group from './group';
import Question from './question';
import Choice from './choice';

export default {
  Poll,
  Group,
  Question,
  Choice,
};
