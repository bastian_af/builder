import CONFIG from './config/app';

const Koa = require('koa');
const KoaRouter = require('koa-router');

const app = new Koa();
const router = new KoaRouter();

router.get('/', async ctx => {
  ctx.body = { data: 'Hello Koajs' };
});

app.use(router.routes()).use(router.allowedMethods());

const server = app.listen(CONFIG.DEFAULT_PORT);
console.log(`Server running on port ${server.address().port} ...`);
